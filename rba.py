#!/usr/bin/python

# Copyright (c) 2013 Denis Cormier <denis.r.cormier@gmail.com>
# See the file license.txt for copying permission

import random
import os
import sys
import argparse


def get_dices(list_of_dices, num_of_dices):
    """At the end of the function, list_of_dices is a list of dices sorted in reverse order."""
    # First, empty the list
    while list_of_dices:
        list_of_dices.pop()
    for n in range(num_of_dices):
        list_of_dices.append(random.randint(1, 6))
    list_of_dices.sort(reverse=True)


def compare_dices(armies, atk_dices, dfd_dices):
    """Play out a round by comparing dices to see who loses armies.

    Input:
    armies['attackers'] -- Number of available defending attackers
    armies['defenders'] -- Number of available defending armies
    atk_dices -- List of attacking dices
    dfd_dices -- List of defending dices

    """
    while atk_dices and dfd_dices:
        if atk_dices[0] > dfd_dices[0]:
            print "Defender loses 1"
            armies['defenders'] -= 1
        else:
            print "Attacker loses 1"
            armies['attackers'] -= 1
        dfd_dices.pop(0)
        atk_dices.pop(0)


def combat_round(armies):
    """Use the maximum amount of dices and conduct an attack.

    Input:
    armies['attackers'] -- Number of available defending attackers
    armies['defenders'] -- Number of available defending armies

    """
    atk_dices = []
    dfd_dices = []

    # Check at start of turn if an attack can take place
    if armies['attackers'] < 2 or armies['defenders'] < 1:
        print "Can't conduct an attack."
        return 1

    if armies['attackers'] == 2:  # 1 attack dice
        get_dices(atk_dices, 1)
    elif armies['attackers'] == 3:  # 2 attack dices
        get_dices(atk_dices, 2)
    else:  # 3 attack dices
        get_dices(atk_dices, 3)

    if armies['defenders'] == 1:  # 1 defense dice
        get_dices(dfd_dices, 1)
    else:  # 2 defense dices
        get_dices(dfd_dices, 2)

    sys.stdout.write("Attacker dices:")
    for i in range(len(atk_dices)):
        sys.stdout.write(" " + str(atk_dices[i]))
    print ""
    sys.stdout.write("Defender dices:")
    for i in range(len(dfd_dices)):
        sys.stdout.write(" " + str(dfd_dices[i]))
    print ""

    compare_dices(armies, atk_dices, dfd_dices)
    return 0


def non_negative(value):
    """Make the parser return an error for a negative value"""
    ivalue = int(value)
    if ivalue < 0:
        raise argparse.ArgumentTypeError("invalid non_negative value: %s" % value)
    return ivalue


def yes_or_no(value):
    """Make the parser return an error for a string that cannot be interpreted as yes or no.

    Return True for an interpretation of yes
    Return False for an interpretation of no
    Raise a TypeError exception and return False for all other cases

    """
    svalue = str(value)
    if svalue == "yes" or svalue == "Yes" or svalue == "YES" or svalue == "y" or svalue == "Y":
        return True
    elif svalue == "no" or svalue == "No" or svalue == "NO" or svalue == "n" or svalue == "N":
        return False
    else:
        raise argparse.ArgumentTypeError("invalid yes_or_no value: %s" % value)
        return False  # In this case, the user could decide to catch the exception and proceed without setting an amount


def main():
    """Run the Risk Battle Automator"""
    print "Risk Battle Automator"
    print "By: Denis Cormier"

    # Parse the command line arguments
    parser = argparse.ArgumentParser(description="Simulate dice rolls for battles occuring in the Risk board game.")
    parser.add_argument("-i", "--interactive", action="store_true",
                        help="Have the program prompt the user for the arguments. Note: all other command line arguments are ignored.")
    parser.add_argument("-a", "--atkarmies", help="number of attacking armies", metavar='N', type=non_negative, default=0)
    parser.add_argument("-d", "--dfdarmies", help="number of defending armies", metavar='N', type=non_negative, default=0)
    parser.add_argument("-n", "--numofattacks", help="number of attacks to perform", metavar='N', type=non_negative)
    #parser.add_argument("-v", "--verbose", help="number of defending armies",
    #                    action="store_true")

    args = parser.parse_args()

    random.seed(os.getpid())  # Pick a seed in a somewhat random fashion
    armies = {'attackers': 0, 'defenders': 0}

    if args.interactive:
        args.atkarmies = non_negative(raw_input("How many armies in the attacking territory? "))
        if args.atkarmies < 2:
            print "Must have at least 2 armies to attack."
            exit()
        args.dfdarmies = non_negative(raw_input("How many armies in the defending territory? "))
        if args.dfdarmies < 1:
            print "Must have at least 1 army to defend."
            exit()
        set_amount = yes_or_no(raw_input("Perform a set amount of attacks? "))
        if set_amount:
            args.numofattacks = non_negative(raw_input("How many? "))
            if args.numofattacks < 1:
                print "Must attack at least once."
                exit()
        else:
            pass

    armies['attackers'] = args.atkarmies
    armies['defenders'] = args.dfdarmies

    if args.numofattacks:
        while armies['attackers'] > 1 and armies['defenders'] > 0 and args.numofattacks > 0:
            combat_round(armies)
            args.numofattacks -= 1
        if args.numofattacks == 0:
            print "Finished the requested number of attacks"
        else:
            print str(args.numofattacks) + " attacks were not conducted."
    else:
        while armies['attackers'] > 1 and armies['defenders'] > 0:
            combat_round(armies)

    if armies['attackers'] < 2:
        print "No more attackers"
    if armies['defenders'] < 1:
        print "No more defenders"

    print "Attacking territory armies left: " + str(armies['attackers'])
    print "Defending territory armies left: " + str(armies['defenders'])

if __name__ == "__main__":
    main()
