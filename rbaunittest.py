#!/usr/bin/python

# Copyright (c) 2013 Denis Cormier <denis.r.cormier@gmail.com>
# See the file license.txt for copying permission

import unittest
from ddt import ddt, data
import rba


@ddt
class Test_get_dices(unittest.TestCase):

    def setUp(self):
        self.dices = []

    def test_order_of_dices(self):
        rba.get_dices(self.dices, 3)
        self.assertTrue(len(self.dices) == 3)
        self.assertTrue(sum(self.dices) >= 3)
        self.assertTrue(sum(self.dices) <= 18)
        self.assertTrue(self.dices[0] >= self.dices[1])
        self.assertTrue(self.dices[1] >= self.dices[2])

    def test_pre_not_empty(self):
        self.dices.append(3)
        self.dices.append(6)
        self.dices.append(5)
        self.dices.append(1)
        rba.get_dices(self.dices, 3)
        self.assertTrue(len(self.dices) == 3)
        self.assertTrue(sum(self.dices) >= 3)
        self.assertTrue(sum(self.dices) <= 18)
        self.assertTrue(self.dices[0] >= self.dices[1])
        self.assertTrue(self.dices[1] >= self.dices[2])

    @data(1, 5, 10, 25, 100)
    def test_dicelist_size(self, value):
        rba.get_dices(self.dices, value)
        self.assertTrue(len(self.dices) == value)
        self.assertTrue(sum(self.dices) >= value)
        self.assertTrue(sum(self.dices) <= 6 * value)


class mylist(list):
    pass


def compare_test_rename(atk_dices, dfd_dices, expected):
    """Change the test name to the form ...x[x][x]_vs_x[x] where x represents a number rolled on a dice."""

    test_values = mylist([atk_dices, dfd_dices, expected])
    if len(atk_dices) == 1 and len(dfd_dices) == 1:
        setattr(test_values, "__name__", "test_compare_dices_%d_vs_%d" % (atk_dices[0], dfd_dices[0]))
    elif len(atk_dices) == 1 and len(dfd_dices) == 2:
        setattr(test_values, "__name__", "test_compare_dices_%d_vs_%d%d" % (atk_dices[0], dfd_dices[0], dfd_dices[1]))
    elif len(atk_dices) == 2 and len(dfd_dices) == 1:
        setattr(test_values, "__name__", "test_compare_dices_%d%d_vs_%d" % (atk_dices[0], atk_dices[1], dfd_dices[0]))
    elif len(atk_dices) == 2 and len(dfd_dices) == 2:
        setattr(test_values, "__name__", "test_compare_dices_%d%d_vs_%d%d" % (atk_dices[0], atk_dices[1], dfd_dices[0], dfd_dices[1]))
    elif len(atk_dices) == 3 and len(dfd_dices) == 1:
        setattr(test_values, "__name__", "test_compare_dices_%d%d%d_vs_%d" % (atk_dices[0], atk_dices[1], atk_dices[2], dfd_dices[0]))
    elif len(atk_dices) == 3 and len(dfd_dices) == 2:
        setattr(test_values, "__name__", "test_compare_dices_%d%d%d_vs_%d%d" % (atk_dices[0], atk_dices[1], atk_dices[2], dfd_dices[0], dfd_dices[1]))
    else:
        print "The parameters passed to compare_test_rename are invalid."

    return test_values


@ddt
class Test_compare_dices(unittest.TestCase):

    def setUp(self):
        self.armies = {'attackers': 0, 'defenders': 0}
        self.atk_dices = []
        self.dfd_dices = []

    @data(compare_test_rename((4,), (4,), (1, 0)), compare_test_rename((5,), (6,), (1, 0)),  # lose 1 attacker
          compare_test_rename((6,), (5,), (0, 1)),  # lose 1 defender
          compare_test_rename((4, 2), (4,), (1, 0)), compare_test_rename((5, 4), (6,), (1, 0)),  # lose 1 attacker
          compare_test_rename((4, 2), (1,), (0, 1)),  # lose 1 defender
          compare_test_rename((4,), (4, 2), (1, 0)), compare_test_rename((5,), (6, 4), (1, 0)),  # lose 1 attacker
          compare_test_rename((4,), (3, 1), (0, 1)),  # lose 1 defender
          compare_test_rename((4, 2, 1), (4,), (1, 0)), compare_test_rename((5, 4, 2), (6,), (1, 0)),  # lose 1 attacker
          compare_test_rename((6, 5, 3), (5,), (0, 1)),  # lose 1 defender
          compare_test_rename((4, 2), (6, 4), (2, 0)), compare_test_rename((4, 2), (4, 2), (2, 0)),  # lose 2 attackers
          compare_test_rename((4, 2), (3, 1), (0, 2)), compare_test_rename((6, 5), (3, 3), (0, 2)),  # lose 2 defenders
          compare_test_rename((4, 2), (6, 1), (1, 1)), compare_test_rename((4, 2), (3, 3), (1, 1)),  # Each lose 1 army
          compare_test_rename((5, 2), (5, 1), (1, 1)), compare_test_rename((6, 3), (4, 3), (1, 1)),  # Each lose 1 army
          compare_test_rename((4, 2, 1), (6, 4), (2, 0)), compare_test_rename((4, 2, 1), (4, 2), (2, 0)),  # lose 2 attackers
          compare_test_rename((4, 2, 1), (3, 1), (0, 2)), compare_test_rename((6, 5, 2), (3, 3), (0, 2)),  # lose 2 defenders
          compare_test_rename((4, 2, 1), (6, 1), (1, 1)), compare_test_rename((4, 2, 1), (3, 3), (1, 1)),  # Each lose 1 army
          compare_test_rename((5, 2, 1), (5, 1), (1, 1)), compare_test_rename((6, 3, 2), (4, 3), (1, 1)))  # Each lose 1 army
    def test_compare_3_atk_1_dfd_dices(self, value):
        atk_tuple, dfd_tuple, expected = value
        expected_lost_attackers, expected_lost_defenders = expected
        init_attackers = 5
        init_defenders = 5
        self.armies['attackers'] = init_attackers
        self.armies['defenders'] = init_defenders

        for i in range(len(atk_tuple)):
            self.atk_dices.append(atk_tuple[i])
        for i in range(len(dfd_tuple)):
            self.dfd_dices.append(dfd_tuple[i])

        print ""  # Newline for cleaner test output
        rba.compare_dices(self.armies, self.atk_dices, self.dfd_dices)

        self.assertTrue(self.armies['attackers'] == init_attackers - expected_lost_attackers)
        self.assertTrue(self.armies['defenders'] == init_defenders - expected_lost_defenders)


@ddt
class Test_combat_round(unittest.TestCase):

    def setUp(self):
        self.armies = {'attackers': 0, 'defenders': 0}

    @data(((1, 1), 1), ((2, 0), 1), ((2, 1), 0), ((5, 5), 0), ((30, 30), 0))
    def test_one_round(self, value):
        init, expected = value
        init_attackers, init_defenders = init
        self.armies['attackers'] = init_attackers
        self.armies['defenders'] = init_defenders
        print ""  # Newline for cleaner test output
        actual = rba.combat_round(self.armies)

        # It is expected that no more than 2 armies between the two parties are lost
        self.assertTrue(self.armies['attackers'] <= init_attackers)
        self.assertTrue(self.armies['defenders'] <= init_defenders)
        self.assertTrue(self.armies['attackers'] + self.armies['defenders'] >= init_attackers + init_defenders - 2)
        # Test whether provided armies allow for a combat round to take place
        self.assertEqual(actual, expected)

if __name__ == '__main__':
    suite_1 = unittest.TestLoader().loadTestsFromTestCase(Test_get_dices)
    suite_2 = unittest.TestLoader().loadTestsFromTestCase(Test_compare_dices)
    suite_3 = unittest.TestLoader().loadTestsFromTestCase(Test_combat_round)
    all_tests = unittest.TestSuite([suite_1, suite_2, suite_3])
    unittest.TextTestRunner(verbosity=2).run(all_tests)
