# Risk battle automator

--------------------

## License

The usage of my code falls under the terms of the MIT/X11 license (see the LICENSE file or see http://opensource.org/licenses/MIT) except otherwise noted.

## About RBA

RBA, the Risk Battle Automator, is a system that simulates dice rolls for battles occuring in Risk. The main purpose is to avoid having to roll dozens of times in a row to resolve a large encounter.

The risk battle automator allows 2 players to quickly automate what could prove to be an excessive amount of dice rolling. By giving the number of attacking and defending armies, the dice rolls can be played out or a set number of attacks can be performed. 

You may also tell the program a fixed amount of attacks to perform.

## Prerequisites

- Python (the language used in this project)
	- It's actually an interpreted (scripting) language. No compilation.
- wxPython
    - This is the graphics library. It is the python version of wxWidgets.
    - http://www.wxpython.org/
- DDT (Data-Driven Tests)
    - http://ddt.readthedocs.org/en/latest/
    - Install the pip package installer first: http://www.pip-installer.org/en/latest/
    - Then, download DDT with the following command: `pip install ddt`
- (Optional) Epydoc automatic documentation generator
    - Uses the "docstrings" of every file, namespace, class, and functions
    - Docstring convention (PEP-257): http://www.python.org/dev/peps/pep-0257/
    - http://epydoc.sourceforge.net/

## Usage

- Use `rba.py -h` or `rba.py --help` for full description of possible inputs

## Some important Python modules/libraries used in my program

- wx: http://wxpython.org/docs/api/
    - wxPython package for creating GUIs
    - Used in rbaprogram.py
- unittest: http://docs.python.org/2/library/unittest.html
    - Python version of JUnit: a unit testing framework
    - Used in rbaunittest.py
- argparse: http://docs.python.org/2/library/argparse.html
    - Allows the configuration of a parser that validates command line arguments
    - Used in rba.py
- random: http://docs.python.org/2/library/random.html
    - Pseudo-random number generator for various distributions
    - Used in rba.py

## Next steps...

- rba.py
    - Implement full usage in parsing and functions
        - When attacking with 1 or 2 dices, give option to retreat
        - Allow "Attack until attacker or defender spends at least x armies"
    - log function that prints to terminal or appends text to an object
        - def log(logobject, message):
        - if logger is none: print message
        - else: append to object
- rbaunittest.py
    - Change tuples to lists
    - Test argument parser
    - Test attacks when providing 0, 1, 2, 3, 4 armies
- GUI with wxPython
    - Package parser in a function for the wxPython program to use
    - Print to logger instead of cmd when executing from wxPython program
    - load .ini file to initialize parameters
    - Use layout managers
