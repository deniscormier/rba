#!/usr/bin/python

# Copyright (c) 2013 Denis Cormier <denis.r.cormier@gmail.com>
# See the file license.txt for copying permission

import wx
import rba
import argparse
import platform


class RBAFrame(wx.Frame):
    """Widget containing the menu and status bar."""
    def __init__(self, my_title, my_size):
        wx.Frame.__init__(self, None, title=my_title, size=my_size)

        self.CreateStatusBar()  # A StatusBar in the bottom of the window

        # Setting up the menu.
        filemenu = wx.Menu()

        # wx.ID_ABOUT and wx.ID_EXIT are standard ids provided by wxWidgets.
        menuAbout = filemenu.Append(wx.ID_ABOUT, "&About", " Information about this program")
        menuExit = filemenu.Append(wx.ID_EXIT, "E&xit", " Terminate the program")

        # Creating the menubar.
        menuBar = wx.MenuBar()
        menuBar.Append(filemenu, "&File")  # Adding the "filemenu" to the MenuBar
        self.SetMenuBar(menuBar)  # Adding the MenuBar to the Frame content.

        # Set events.
        self.Bind(wx.EVT_MENU, self.OnAbout, menuAbout)
        self.Bind(wx.EVT_MENU, self.OnExit, menuExit)

    def OnAbout(self, event):
        # A message dialog box with an OK button. wx.OK is a standard ID in wxWidgets.
        dlg = wx.MessageDialog(self, "By Denis Cormier", "Risk Battle Automator", wx.OK)
        dlg.ShowModal()  # Show it
        dlg.Destroy()  # finally destroy it when finished.

    def OnExit(self, event):
        self.Close(True)  # Close the frame.


class RBAPanel(wx.Panel):
    """Widget containing the app's basic functionality."""
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        self.atk_armies_label = wx.StaticText(self, label="Attacking armies:", pos=(20, 20))
        self.dfd_armies_label = wx.StaticText(self, label="Defending armies:", pos=(20, 50))
        self.num_of_attacks_label = wx.StaticText(self, label="Number of attacks:", pos=(20, 80))
        # A multiline TextCtrl to show the dice rolls
        self.logger = wx.TextCtrl(self, pos=(370, 20), size=(320, 300), style=wx.TE_MULTILINE | wx.TE_READONLY)

        # Textboxes are positioned a bit differently depending on the OS
        if platform.system() == "Linux":
            self.atk_armies_input = wx.TextCtrl(self, pos=(150, 15), size=(50, -1))
            self.dfd_armies_input = wx.TextCtrl(self, pos=(150, 45), size=(50, -1))
            self.num_of_attacks_input = wx.TextCtrl(self, pos=(150, 75), size=(50, -1))
        else:
            self.atk_armies_input = wx.TextCtrl(self, pos=(120, 15), size=(50, -1))
            self.dfd_armies_input = wx.TextCtrl(self, pos=(120, 45), size=(50, -1))
            self.num_of_attacks_input = wx.TextCtrl(self, pos=(120, 75), size=(50, -1))

        # A button to execute the simulation
        self.button_exec = wx.Button(self, label="Execute", pos=(270, 350))
        self.Bind(wx.EVT_BUTTON, self.OnClickExecute, self.button_exec)

        # A button to clear the log
        self.button_clear = wx.Button(self, label="Clear", pos=(370, 350))
        self.Bind(wx.EVT_BUTTON, self.OnClickClear, self.button_clear)

    def ErrorPopup(self, message):
        # A message dialog box with an OK button. wx.OK is a standard ID in wxWidgets.
        dlg = wx.MessageDialog(self, message, "Error", wx.OK)
        dlg.ShowModal()  # Show it
        dlg.Destroy()  # finally destroy it when finished.

    def OnClickExecute(self, event):
        """Execute the combat rounds via the program"""
        armies = {'attackers': 0, 'defenders': 0}
        try:
            armies['attackers'] = rba.non_negative(self.atk_armies_input.GetLineText(0))
            armies['defenders'] = rba.non_negative(self.dfd_armies_input.GetLineText(0))
            num_of_attacks = rba.non_negative(self.num_of_attacks_input.GetLineText(0))
            self.logger.AppendText("Executing the attacks...\n")
        except argparse.ArgumentTypeError:
            self.ErrorPopup("All arguments must be non-negative.")
        except ValueError:
            self.ErrorPopup("An argument is missing.")

        while armies['attackers'] > 1 and armies['defenders'] > 0 and num_of_attacks > 0:
            rba.combat_round(armies)
            num_of_attacks -= 1
        if num_of_attacks == 0:
            self.logger.AppendText("Finished the requested number of attacks\n")
        else:
            self.logger.AppendText(str(num_of_attacks) + " attacks were not conducted.\n")
        self.logger.AppendText("Attackers left: %d\n" % armies['attackers'])
        self.logger.AppendText("Defenders left: %d\n" % armies['defenders'])

    def OnClickClear(self, event):
        self.logger.SetValue("")

    def EvtCheckBox(self, event):
        self.logger.AppendText('EvtCheckBox: %d\n' % event.Checked())


if __name__ == '__main__':
    app = wx.App(False)
    frame = RBAFrame("Risk Battle Automator", (720, 480))
    panel = RBAPanel(frame)
    frame.Show()
    app.MainLoop()
